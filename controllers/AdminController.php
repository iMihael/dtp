<?php

namespace app\controllers;

use app\models\Dtp;
use app\models\Task;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\UploadedFile;

class AdminController extends Controller {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'import', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'import', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {



        return $this->render('index', [
            'taskCount' => Task::find()->where(['status' => Task::STATUS_QUEUE])->count(),
            'dataProvider' => new ActiveDataProvider([
                'query' => Dtp::find(),
            ]),
        ]);

    }

    //Action for demo purposes
    public function actionDelete() {
        Dtp::deleteAll();
        return $this->redirect(['admin/index']);
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        $this->redirect(['site/index']);
    }

    public function actionLogin() {
        if(!Yii::$app->user->isGuest) {
            return $this->redirect(['admin/index']);
        }

        $model = new User();
        $model->setScenario('login');

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->redirect(['admin/index']);
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionImport() {

        if($file = UploadedFile::getInstanceByName('import')) {
            Task::newFile($file->tempName);
            return $this->render('import', [
                'queue' => true,
            ]);
        }

        return $this->render('import');
    }
}