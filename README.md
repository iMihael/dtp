Ukrainian Web Challenge, part 2
=================================

Website:
[http://dtp.mihael.me/UWCUA.zip](http://dtp.mihael.me/UWCUA.zip)
[http://dtp.mihael.me](http://dtp.mihael.me)  

Install steps:

1. Clone project and cd into it;
2. Run `composer install` and `npm install`
3. Go to web/js
4. Run `bower install`
5. chmod 777 web/assets -R
6. chmod 777 runtime/ -R
7. Create database, configure config/db.php
8. run `./yii migrate/up` (or import sql dump from project tree (dtp.sql))
9. Configure web server to look into web/
10. To properly implement import functionality please add cron task (every minute) for command `./yii task`:
`0-59 * * * * /path/to/yii task`
