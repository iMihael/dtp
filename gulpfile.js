var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

var conf = require('./package.json');

var srcPath = 'web/js/src';
var dstPath = 'web/js';

gulp.task(conf.env == 'dev' ? 'js' : 'default', function () {
    var app = gulp.src([

        srcPath + '/**/module.js',
        srcPath + '/app/**/*.js'
    ])
        .pipe(concat('app.js'));

    if( conf.env == 'prod' ) {
        app.pipe(uglify({mangle: false}));
    }

    app.pipe(gulp.dest(dstPath));
});

if(conf.env == 'dev') {
    gulp.task('default', ['js'], function () {
        gulp.watch(srcPath + '/**/*.js', ['js']);
    });
}