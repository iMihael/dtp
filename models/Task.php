<?php

namespace app\models;
use yii\db\ActiveRecord;

class Task extends ActiveRecord {

    const STATUS_QUEUE = 'queue';
    const STATUS_PROGRESS = 'progress';
    const STATUS_DONE = 'done';
    const STATUS_FAILED = 'failed';

    public static function newFile($path) {
        $t = new self();

        $newPath = __DIR__ . '/../runtime/cache/' . sha1(time() . mt_rand(0, 100));
        move_uploaded_file($path, $newPath);

        $t->setAttributes([
            'fileName' => $newPath,
            'status' => self::STATUS_QUEUE,
        ]);
        $t->save();
    }

    public function rules() {
        return [
            ['fileName', 'safe'],
            ['status', 'in', 'range' => [self::STATUS_QUEUE, self::STATUS_DONE, self::STATUS_FAILED, self::STATUS_PROGRESS]],
        ];
    }

    public static function tableName() {
        return 'task';
    }
}