<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\ForbiddenHttpException;
use Yii;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    public static function tableName() {
        return 'user';
    }

    public function rules() {
        return [
            [['email', 'password'], 'required', 'on' => 'login'],
            ['email', 'email'],
            ['email', 'validateEmail', 'on' => 'login'],
        ];
    }

    public function validateEmail($attr) {
        if($user = self::findOne(['email' => $this->$attr])) {
            if(Yii::$app->security->validatePassword($this->password, $user->password)) {
                Yii::$app->user->login($user);
                return true;
            }
        }

        $this->addError($attr, 'Wrong email');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new ForbiddenHttpException();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return false;
    }

    public function attributeLabels() {
        return [
            'email' => 'Електронна пошта',
            'password' => 'Пароль'
        ];

    }
}
