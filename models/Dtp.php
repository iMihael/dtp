<?php

namespace app\models;

use yii\db\ActiveRecord;

class Dtp extends ActiveRecord {

    const VICTIM_YES = 1;
    const VICTIM_NO = 0;

    public static function tableName() {
        return 'dtp';
    }

    public function rules() {
        return [
            [['eventDate', 'event', 'latitude', 'longitude'], 'required', 'on' => 'import'],
            ['event', 'in', 'range' => [self::VICTIM_YES, self::VICTIM_NO]],
            [['latitude', 'longitude'], 'integer', 'integerOnly' => false],
            ['eventDate', 'match', 'pattern' => '/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/'],
        ];
    }

    public static function routeFilter($route) {
        $minLt = $route[0]['lat'];
        $maxLt = $route[0]['lat'];

        $minLg = $route[0]['lng'];
        $maxLg = $route[0]['lng'];

        foreach($route as $r) {
            if($r['lat'] > $maxLt) {
                $maxLt = $r['lat'];
            }

            if($r['lat'] < $minLt) {
                $minLt = $r['lat'];
            }

            if($r['lng'] > $maxLg) {
                $maxLg = $r['lng'];
            }

            if($r['lng'] < $minLg) {
                $minLg = $r['lng'];
            }
        }

        return self::find()
            ->where(['between', 'latitude', $minLt, $maxLt])
            ->andWhere(['between', 'longitude', $minLg, $maxLg]);
    }

    public static function importData($filePath) {
        $f = fopen($filePath, 'r');

        fgetcsv($f);
        while(!feof($f)) {
            $line = fgetcsv($f);

            $date = new \DateTime($line[0]);
            $data = [
                'eventDate' => $date->format('Y-m-d H:i:s'),
                'event' => $line[1] == 'ДТП З ПОТЕРПIЛИМИ' ? self::VICTIM_YES : self::VICTIM_NO,
                'latitude' => $line[3],
                'longitude' => $line[2],
            ];

            if($dtp = Dtp::findOne($data)) {
                continue;
            }

            $dtp = new Dtp();
            $dtp->setScenario('import');
            $dtp->setAttributes($data);
            $dtp->save();
        }

        fclose($f);
        unlink($filePath);
    }

    public function attributeLabels() {
        return [
            'event' => 'Подія',
            'eventDate' => 'дата повід.102',
            'latitude' => 'Широта',
            'longitude' => 'Довгота',
        ];
    }
}