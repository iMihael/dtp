<?php

use yii\db\Schema;
use yii\db\Migration;
use app\models\Task;

class m151024_190522_task extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Task::tableName(), [
            'id' => Schema::TYPE_PK,
            'fileName' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_STRING,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(Task::tableName());
    }


}
