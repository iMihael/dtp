<?php

use yii\db\Schema;
use yii\db\Migration;
use app\models\Dtp;
use app\models\User;

class m151024_042057_dtp extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Dtp::tableName(), [
            'id' => Schema::TYPE_PK,
            'eventDate' => Schema::TYPE_DATETIME,
            'event' => Schema::TYPE_STRING,
            'latitude' => Schema::TYPE_STRING,
            'longitude' => Schema::TYPE_STRING,
        ], $tableOptions);

        $this->createIndex('dtp_unq', Dtp::tableName(), ['eventDate', 'event', 'latitude', 'longitude'], true);

        $this->createTable(User::tableName(), [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING,
            'password' => Schema::TYPE_STRING,
        ], $tableOptions);

        $this->insert(User::tableName(), [
            'email' => 'admin@admin.com',
            'password' => Yii::$app->security->generatePasswordHash('123456'),
        ]);
    }

    public function down()
    {
        $this->dropTable(Dtp::tableName());
        $this->dropTable(User::tableName());
    }

}
