<?php

namespace app\modules\v1\controllers;

use yii\data\ActiveDataProvider;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use Yii;
use app\models\Dtp;

class DtpController extends ActiveController {
    public $modelClass = 'app\models\Dtp';

    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON
                ],
            ]
        ];
    }

    public function actionRoute() {
        $route = Yii::$app->request->post('route');

        return new ActiveDataProvider([
            'query' => Dtp::routeFilter($route),
            'pagination' => false,
        ]);
    }

    public function checkAccess($action, $model = null, $params = []) {
        if($action != 'index') {
            throw new ForbiddenHttpException();
        }
    }
}