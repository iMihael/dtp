<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'js/vendor/bower/angular-growl-v2/build/angular-growl.min.css',
        'js/vendor/bower/angular-loading-bar/build/loading-bar.min.css',
    ];
    public $js = [
        'js/vendor/bower/angular/angular.min.js',
        'js/vendor/bower/angular-resource/angular-resource.min.js',
        'js/vendor/bower/angular-route/angular-route.min.js',
        'js/vendor/bower/angular-bootstrap/ui-bootstrap.min.js',
        'js/vendor/bower/angular-growl-v2/build/angular-growl.min.js',
        'js/vendor/bower/angular-loading-bar/build/loading-bar.min.js',
        'js/app.js'
    ];
}