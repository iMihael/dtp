<?php

namespace app\commands;

use app\models\Dtp;
use app\models\Task;
use yii\console\Controller;

class TaskController extends Controller {

    public function actionIndex() {
        if($t = Task::findOne(['status' => Task::STATUS_QUEUE])) {
            $t->status = Task::STATUS_PROGRESS;
            $t->save();

            try {
                Dtp::importData($t->fileName);
                $t->status = Task::STATUS_DONE;
                $t->save();
            } catch (\Exception $e) {
                $t->status = Task::STATUS_FAILED;
                $t->save();
            }
        }
    }
}