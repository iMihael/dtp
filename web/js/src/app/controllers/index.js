angular.module('app').controller('indexController',  ['$scope', 'dtp', function($scope, dtp){


    $scope.map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 50.448650,
            lng: 30.522388
        },
        zoom: 8
    });

    $scope.directionsDisplay = new google.maps.DirectionsRenderer();
    $scope.directionsDisplay.setMap($scope.map);

    $scope.directionsService = new google.maps.DirectionsService();

    $scope.from = document.getElementById('from');
    $scope.to = document.getElementById('to');

    var fromAutoComplete = new google.maps.places.Autocomplete($scope.from);
    var toAutoComplete = new google.maps.places.Autocomplete($scope.to);

    $scope.interval = 0.0005;
    $scope.routes = [];
    $scope.routeNum = 0;
    $scope.handledRoutes = [];

    $scope.setRoute = function(num){

        var color;

        for(var h in $scope.handledRoutes) {
            if($scope.handledRoutes[h].num == num) {
                $scope.handledRoutes[h].selected = true;

                for(var i in $scope.handledRoutes[h].markers) {
                    $scope.handledRoutes[h].markers[i].setMap($scope.map);
                }

                color = $scope.handledRoutes[h].color;

            } else {
                $scope.handledRoutes[h].selected = false;

                for(var i in $scope.handledRoutes[h].markers) {
                    $scope.handledRoutes[h].markers[i].setMap(null);
                }
            }
        }


        $scope.directionsDisplay.setOptions({
            polylineOptions: {
                strokeColor: color,
                strokeWeight: 5
            }
        });

        $scope.directionsDisplay.setRouteIndex(num - 1);
    };

    $scope.handleRoute = function(){
        $scope.routeNum++;
        if($scope.routes.length > 0) {
            var route = $scope.routes[0];
            var routePoints = [];

            for(var j in route.overview_path) {
                routePoints.push({
                    lat: route.overview_path[j].lat(),
                    lng: route.overview_path[j].lng()
                });
            }

            dtp.route({
                route: routePoints
            }, function(data){

                var hRoute = {
                    markers: [],
                    num: $scope.routeNum,
                    selected: false,
                    rate: 0
                };

                for(var d in data) {

                    if (data[d].hasOwnProperty('latitude')) {

                        var render = false;

                        var lat = parseFloat(data[d].latitude);
                        var lng = parseFloat(data[d].longitude);

                        for(var rp in routePoints) {
                            if(
                                routePoints[rp].lat - $scope.interval < lat && routePoints[rp].lat + $scope.interval > lat &&
                                routePoints[rp].lng - $scope.interval < lng && routePoints[rp].lng + $scope.interval > lng
                            ) {
                                render = true;
                            }
                        }

                        if(render) {
                            var myLatLng = {
                                lat: lat,
                                lng: lng
                            };

                            var marker = new google.maps.Marker({
                                position: myLatLng
                            });

                            hRoute.rate += parseInt(data[d].event) + 1;

                            hRoute.markers.push(marker);
                        }


                    }
                }

                $scope.handledRoutes.push(hRoute);

                $scope.routes.splice(0, 1);
                if($scope.routes.length > 0) {
                    $scope.handleRoute();
                } else {

                    $scope.handledRoutes.sort(function(a, b){
                        if(a.rate < b.rate) return -1;
                        if(a.rate > b.rate) return 1;
                        return 0;
                    });

                    if($scope.handledRoutes.length > 0) {
                        $scope.handledRoutes[0].color = '#68E882';
                    }

                    if($scope.handledRoutes.length > 1) {
                        $scope.handledRoutes[1].color = '#F5EE1D';
                    }

                    if($scope.handledRoutes.length > 2) {
                        $scope.handledRoutes[2].color = '#F55F78';
                    }

                    for(var i = 3;i<$scope.handledRoutes.length;i++) {
                        $scope.handledRoutes[i].color = '#F55F78';
                    }

                    $scope.setRoute(1);
                }
            });
        }
    };

    $scope.calcRoute = function(){

        var from = $scope.from.value;
        var to = $scope.to.value;
        if($scope.handledRoutes.length > 0) {
            for(var i in $scope.handledRoutes) {
                for(var j in $scope.handledRoutes[i].markers) {
                    $scope.handledRoutes[i].markers[j].setMap(null);
                }
            }
        }
        $scope.handledRoutes = [];
        $scope.routes = [];
        $scope.routeNum = 0;

        //from = 'Хрещатик, Київ, місто Київ, Україна';
        //to = 'Вишгород, Київська область, Україна';

        if(from && to) {

            var request = {
                origin: from,
                destination: to,
                travelMode: google.maps.TravelMode.DRIVING,
                provideRouteAlternatives: true
            };

            $scope.directionsService.route(request, function (result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    $scope.directionsDisplay.setDirections(result);

                    $scope.routes = result.routes;
                    $scope.handleRoute();
                }
            });
        }


    };
}]);