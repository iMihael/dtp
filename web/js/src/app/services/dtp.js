angular.module('app').factory('dtp', ['$resource', function($resource) {
    return $resource('/v1/dtp', {}, {
        route: {
            method: 'POST',
            url: '/v1/dtp/route',
            isArray: true
        }
    });
}]);