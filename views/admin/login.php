<?php
    $this->title = 'Вхiд';
?>

<div class="col-lg-offset-4 col-lg-4 panel panel-default">

    <div class="panel-heading">
        <h2>Вхiд</h2>
    </div>
    <div class="panel-body">
<?php

    $form = \yii\widgets\ActiveForm::begin();
    echo $form->field($model, 'email')->hint('Демо вхiд: admin@admin.com');
    echo $form->field($model, 'password')->passwordInput()->hint('Демо вхiд: 123456');

?>
        <div class="form-group">
            <input type="submit" class="btn btn-primary form-control" value="Вхiд" />
        </div>
    </div>

</div>