<?php
    use yii\helpers\Url;
    use yii\grid\GridView;
    use app\models\Dtp;

    $this->title = 'Список ДТП';
?>

<h2>Список ДТП</h2>
<p>
    <a class="btn btn-primary" href="<?php echo Url::toRoute(['admin/import']) ?>">Iмпорт</a>
    <a onclick="confirm('Ви впевненi ?')" class="btn btn-danger" href="<?php echo Url::toRoute(['admin/delete']) ?>">Видалити все</a>
</p>

<?php if($taskCount > 0): ?>

    <div class="alert alert-info">
        <p>На разi в черзi знаходится файлiв: <b><?php echo $taskCount ?></b></p>
        <p>На протязi 1-2 хвилин почнеться обробка...</p>
    </div>

<?php endif;?>

<?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        [
            'attribute' => 'event',
            'value' => function($model){
                return $model->event == Dtp::VICTIM_YES ? 'ДТП З ПОТЕРПIЛИМИ' : 'ДТП БЕЗ ПОТЕРПIЛИХ';
            },
        ],
        [
            'attribute' => 'eventDate',
            'value' => function($model) {
                $dt = new DateTime($model->eventDate);
                return $dt->format('d.m.Y H:i:s');
            }
        ],
        'latitude',
        'longitude'
    ],
]); ?>
