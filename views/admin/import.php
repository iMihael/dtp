<?php
    $this->title= 'Iмпорт';
    use yii\helpers\Url;
?>

<div class="panel panel-default col-lg-offset-3 col-lg-6">
    <div class="panel-heading">
        <h2>Iмпорт</h2>
    </div>
    <div class="panel-body">
        <form method="POST" enctype="multipart/form-data">

            <?php if(isset($queue)): ?>
                <div class="alert alert-info">
                    Файл успiшно доданий до черги iмпорту. Протягом 2-3 хвилин, файл буде повнiстью iмпортований. Новi даннi ви можете переглянути на
                    <b><a href="<?php echo Url::toRoute(['admin/index']) ?>">головнiй сторiнцi</a></b>.
                </div>
            <?php endif; ?>

            <input type="hidden" name="_csrf" value="<?php echo Yii::$app->request->csrfToken ?>" />

            <div class="form-group">
                <label>Оберiть csv файл для iмпорту</label>
                <input type="file" name="import" class="form-control" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary form-control" value="Iмпорт" />
            </div>
        </form>
    </div>
</div>



